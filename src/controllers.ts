import { Request, Response } from "express";
import fetch from "node-fetch";

const forwardRequestToGraphQL = async (
  query: string,
  variables: any,
  req: Request,
  res: Response
) => {
  const result = await fetch("http://localhost:4181/graphql", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      query: query,
      variables: variables,
    }),
  }).then((r) => r.json());

  if ("errors" in result) {
    const message = result["errors"][0]["message"];
    return res.status(400).send(message);
  }

  res.status(200).send(result);
};

export const getProduct = async (req: Request, res: Response) => {
  forwardRequestToGraphQL(
    `
    query ($productId: ID!) {
      product(id: $productId) {
        name
        id
        createdAt
        tags {
          name
          id
        }
      }
    }`,
    { productId: req.params.id },
    req,
    res
  );
};

export const getProducts = (req: Request, res: Response) => {
  forwardRequestToGraphQL(
    `
    query ($productsPaginationInput: OrderedPaginationInput, $productsTag: String) {
      products(paginationInput: $productsPaginationInput, tag: $productsTag) {
        name
        id
        createdAt
        tags {
          name
          id
        }
      }
    }`,
    {
      productsPaginationInput: {
        limit: req.query.limit
          ? parseInt(req.query.limit as string)
          : undefined,
        offset: req.query.offset
          ? parseInt(req.query.offset as string)
          : undefined,
        order: req.query.order,
        orderBy: req.query.orderby,
      },
      productsTag: req.query.tag,
    },
    req,
    res
  );
};

export const postProduct = (req: Request, res: Response) => {
  forwardRequestToGraphQL(
    `
    mutation($createProductName: String!, $createProductTags: [String!]) {
      createProduct(name: $createProductName, tags: $createProductTags) {
        name
        id
        createdAt
        tags {
          name
          id
        }
      }
    }`,
    {
      createProductName: req.params.name,
      createProductTags: req.query.tags
        ? (req.query.tags as string).split(",")
        : undefined,
    },
    req,
    res
  );
};

export const putProduct = (req: Request, res: Response) => {
  forwardRequestToGraphQL(
    `
    mutation($updateProductId: ID!, $updateProductName: String, $updateProductTags: [String!]){
      updateProduct(id: $updateProductId, name: $updateProductName, tags: $updateProductTags) {
        name
        id
        createdAt
        tags {
          name
          id
        }
      }
    }`,
    {
      updateProductId: req.params.id,
      updateProductName: req.query.name,
      updateProductTags: req.query.tags
        ? (req.query.tags as string).split(",")
        : undefined,
    },
    req,
    res
  );
};
