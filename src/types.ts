import { registerEnumType } from "type-graphql";

export enum Order {
  asc = "ASC",
  desc = "DESC",
}

export enum OrderBy {
  name = "name",
  createdat = "createdAt",
}

registerEnumType(Order, {
  name: "Order",
  description: "Whether to order something ascending or descending",
  valuesConfig: {
    asc: {
      description: "Ascending",
    },
    desc: {
      description: "Descending",
    },
  },
});

registerEnumType(OrderBy, {
  name: "OrderBy",
  description: "What property to order by",
  valuesConfig: {
    name: {
      description: "Order by name",
    },
    createdat: {
      description: "Order by creation date",
    },
  },
});
