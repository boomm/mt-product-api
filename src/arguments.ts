import { IsEnum, IsInt, Min, Max } from "class-validator";
import { InputType, Field, Int } from "type-graphql";
import { Order, OrderBy } from "./types";

@InputType({
  description: "arguments to set the pagination via offset and limit.",
})
export class PaginationInput {
  @IsInt()
  @Min(0)
  @Field(() => Int, {
    defaultValue: 0,
    description: "How many objects to skip while retrieving.",
  })
  offset: number = 0;

  @IsInt()
  @Min(1)
  @Max(50)
  @Field(() => Int, {
    defaultValue: 10,
    description: "How many objects to retrieve. Max 50.",
  })
  limit: number = 10;
}

@InputType({
  description:
    "arguments to set the pagination via offset and limit, in addition to setting the ordering.",
})
export class OrderedPaginationInput extends PaginationInput {
  @IsEnum(Order)
  @Field(() => Order, {
    defaultValue: Order.desc,
    description:
      "What order the objects should be retrieved in. One of asc or desc.",
  })
  order: Order = Order.desc;

  @IsEnum(OrderBy)
  @Field(() => OrderBy, {
    defaultValue: OrderBy.createdat,
    description: "What to order the objects by. One of name or createdat.",
  })
  orderBy: OrderBy = OrderBy.createdat;
}
