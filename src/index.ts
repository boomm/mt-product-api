import "reflect-metadata"; // typeorm & type-graphl rely on this import
import express from "express";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import { createConnection } from "typeorm";
import { port, connectionOptions } from "./config";
import { ProductResolver, TagResolver } from "./resolvers";
import {
  getProduct,
  getProducts,
  postProduct,
  putProduct,
} from "./controllers";

const main = async () => {
  await createConnection(connectionOptions);

  const app = express();

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [ProductResolver, TagResolver],
      validate: true,
    }),
  });

  await apolloServer.start();
  apolloServer.applyMiddleware({ app });

  app.listen(port, () => {
    console.log(`Running server on port ${port}`);
  });

  app.get("/product/:id", getProduct);
  app.get("/products", getProducts);
  app.post("/product/:name", postProduct);
  app.put("/product/:id", putProduct);
};

main();
