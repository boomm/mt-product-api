import { ConnectionOptions } from "typeorm";
import { Product, Tag } from "./entities";

const host = process.env.DB_HOST || "127.0.0.1";
const username = process.env.DB_USERNAME || "postgres";
const password = process.env.DB_PASSWORD || "";
export const port = process.env.NODE_PORT || 4181;

export const connectionOptions: ConnectionOptions = {
  type: "postgres",
  host: host,
  extra: {
    socketPath: host,
  },
  database: "mt_assignment",
  username: username,
  password: password,
  logging: true,
  synchronize: true,
  entities: [Product, Tag],
};
