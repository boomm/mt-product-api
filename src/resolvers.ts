import { Product, Tag } from "./entities";
import { Arg, Query, Mutation, Resolver, ID } from "type-graphql";
import { getConnection } from "typeorm";
import { PaginationInput, OrderedPaginationInput } from "./arguments";
import { UserInputError } from "apollo-server-express";

// ----------------------------------------------------------------
// PRODUCT
// ----------------------------------------------------------------

@Resolver()
export class ProductResolver {
  @Query(() => [Product], {
    description: "Get a collection of products, optionally filtered by tag.",
  })
  async products(
    @Arg("paginationInput", { defaultValue: new OrderedPaginationInput() })
    input: OrderedPaginationInput,
    @Arg("tag", { nullable: true })
    tagName: string
  ): Promise<Product[]> {
    // Get all products

    console.log(input);
    // get products and tags
    const qb = getConnection()
      .getRepository(Product)
      .createQueryBuilder("product")
      .leftJoinAndSelect("product.tags", "tags");

    if (tagName) {
      // filter on tag if parameter is given
      qb.leftJoin("product.tags", "tagFilter").where("tagFilter.name = :tag", {
        tag: tagName,
      });
    }

    // order and paginate
    return qb
      .orderBy(`product.${input.orderBy}`, input.order)
      .skip(input.offset)
      .take(input.limit)
      .getMany();
  }

  @Query(() => Product, {
    description: "Get a single product matching the id.",
    nullable: true,
  })
  async product(@Arg("id", () => ID) id: string): Promise<Product | undefined> {
    // Get a product by its unique id

    return Product.findOne(id);
  }

  @Mutation(() => Product, {
    description: "Create a single product. The name needs to be unique.",
  })
  async createProduct(
    @Arg("name", () => String) name: string,
    @Arg("tags", () => [String], { nullable: true }) tagnames: string[]
  ): Promise<Product> {
    // Create a new product with a unique name and optional tags

    const product = Product.create({ name });

    if (typeof tagnames !== "undefined")
      product.tags = await getOrCreateTagsByname(tagnames);

    return product.save();
  }

  @Mutation(() => Product, {
    description:
      "Update the name and/or tags of a product. The name needs to be unique.",
    nullable: true,
  })
  async updateProduct(
    @Arg("id", () => ID) id: string,
    @Arg("name", () => String, { nullable: true }) name: string,
    @Arg("tags", () => [String], { nullable: true }) tagNames: string[]
  ): Promise<Product | undefined> {
    // Update a product's unique name and/or tags by its ID

    const product = await Product.findOne(id);

    if (!product) throw new UserInputError(`product with id ${id} not found`);

    if (typeof name !== "undefined") product.name = name;
    if (typeof tagNames !== "undefined")
      product.tags = await getOrCreateTagsByname(tagNames);
    return product.save();
  }
}

// ----------------------------------------------------------------
// TAG
// ----------------------------------------------------------------

async function getOrCreateTagsByname(tagNames: string[]) {
  // Find and return tags based on their unique name
  // Create any that do not yet exist

  const tags = await Promise.all(
    tagNames.map(
      async (name) =>
        (await Tag.findOne({ where: { name } })) || Tag.create({ name }).save()
    )
  );

  return tags;
}

async function getOrCreateTagByname(tagName: string) {
  // Find and return a single tag based on its unique name
  // Create it if it does not yet exist

  return (await getOrCreateTagsByname([tagName]))[0];
}

@Resolver()
export class TagResolver {
  @Query(() => [Tag], {
    description: "Get all tags.",
  })
  async tags(
    @Arg("paginationInput", { defaultValue: new PaginationInput() })
    input: PaginationInput
  ): Promise<Tag[]> {
    // Get all tags

    return getConnection()
      .getRepository(Tag)
      .createQueryBuilder("tag")
      .skip(input.offset)
      .take(input.limit)
      .getMany();
  }

  @Query(() => Tag, { nullable: true })
  async tag(@Arg("id", () => ID) id: string): Promise<Tag | undefined> {
    // Get a tag by its unique id

    return Tag.findOne(id);
  }

  @Mutation(() => Tag)
  async createTag(@Arg("name", () => String) name: string): Promise<Tag> {
    // Create a new tag with a unique name or fetch an existing one

    const tag = await getOrCreateTagByname(name);

    return tag;
  }

  @Mutation(() => Tag, { nullable: true })
  async updateTag(
    @Arg("id", () => ID)
    id: string,
    @Arg("name", () => String) name: string
  ): Promise<Tag | undefined> {
    // Update a tag's unique name by its ID

    const tag = await Tag.findOne(id);

    if (!tag) throw new UserInputError(`tag with id ${id} not found`);

    tag.name = name;
    return tag.save();
  }
}
