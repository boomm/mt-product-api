import { ObjectType, Field, ID } from "type-graphql";
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from "typeorm";

@ObjectType()
@Entity()
abstract class Identifiable extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn("uuid")
  readonly id!: string;

  @Field()
  @Column({ unique: true })
  name!: string;
}

@ObjectType({ description: "A generic product" })
@Entity()
export class Product extends Identifiable {
  @Field(() => [Tag], { nullable: true })
  @ManyToMany(() => Tag, {
    cascade: true,
    eager: true,
    orphanedRowAction: "delete",
  })
  @JoinTable()
  tags: Tag[];

  @Field(() => String)
  @CreateDateColumn()
  readonly createdAt!: Date;
}

@ObjectType({
  description: "A tag to classify products.",
})
@Entity()
export class Tag extends Identifiable {}
