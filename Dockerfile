# Step 1: build the project
FROM node:latest as ts-compiler
WORKDIR /app
COPY package.json ./
RUN npm install
COPY tsconfig.json ./
COPY ./src ./src
RUN npm run build

# Step 2: only keep the transpiled JS code
FROM node:16-alpine as ts-remover
WORKDIR /app
COPY --from=ts-compiler /app/package.json ./
COPY --from=ts-compiler /app/dist ./dist
RUN npm install --only=production

# Step 3: transfer to distroless image
FROM gcr.io/distroless/nodejs:14
WORKDIR /app
COPY --from=ts-remover /app ./
CMD ["dist/index.js"]
